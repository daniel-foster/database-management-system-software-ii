module software.DMS {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires jdk.internal.le;
    requires org.jetbrains.annotations;


    opens software.DMS to javafx.fxml;
    exports software.DMS;
}