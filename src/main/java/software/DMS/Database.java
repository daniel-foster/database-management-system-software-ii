package software.DMS;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Class that manages the connection to and queries from the database.
 */
public class Database {

    /**
     * An exception that is thrown when there is no data returned from the database.
     */
    public static class DoesNotExistException extends Exception {
        /**
         * Constructor.
         * @param s A helpful string.
         */
        public DoesNotExistException(String s) {
            super(s);
        }
    }

    /**
     * An exception thrown when login has failed.
     */

    public static class LoginFailedException extends Exception {
        /**
         * Constructor.
         * @param s A helpful string.
         */
        public LoginFailedException(String s) {
            super(s);
        }
    }

    private static final ObservableList<Appointment> appt = FXCollections.observableArrayList();
    private static final ObservableList<Customer> cust = FXCollections.observableArrayList();
    private static final ObservableList<Country> country_list = FXCollections.observableArrayList();
    private static final ObservableList<Contact>contact_list = FXCollections.observableArrayList();
    private static Connection con;
    private static Statement st;

    /**
     * Initializes the connection to the database.
     * @throws SQLException if the database connection fails.
     */

    public static void connect() throws SQLException {
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/client_schedule?user=sqlUser&password=Passw0rd!");
        st = con.createStatement();
    }

    /**
     * Logs into a MySQL database.
     * @param user the username for the database login.
     * @param password the password for the database login.
     * @throws SQLException if the database connection fails.
     * @throws LoginFailedException If the username or password fails.
     */

    public static void login(String user, String password) throws SQLException, LoginFailedException {
        ArrayList<ArrayList<Object>> res = get_all("SELECT * FROM users WHERE User_Name = '" + user + "' AND Password = '" + password + "'");
        if (res.size() != 1) {
            throw new LoginFailedException("Username or password is incorrect");
        }
    }

    /**
     * A function to return a list of strings from the appointment class based on a provided lambda.
     * @param f Lambda for the string to be returned.
     * @return A list of strings.
     */
    public static ObservableList<String> getAppStr(AppointmentFunction.Str f) {
        ObservableList<String> l = FXCollections.observableArrayList();

        appt.forEach(a -> l.add(f.strFn(a)));

        return l;
    }

    /**
     * A function to return a list of LocalDateTimes from the appointment class based on a provided lambda.
     * @param f Lambda for the LocalDateTime to be returned.
     * @return A list of LocalDateTimes.
     */
    public static ObservableList<LocalDateTime> getAppLdt(AppointmentFunction.ldt f) {
        ObservableList<LocalDateTime> l = FXCollections.observableArrayList();

        appt.forEach(a -> l.add(f.ldtFn(a)));

        return l;
    }

    /**
     * Queries the database for the largest appointment id.
     * @return the id int.
     * @throws SQLException if there is an error querying the database.
     */
    public static int getApptId() throws SQLException {

        return (int) get_all("SELECT MAX(Appointment_ID) FROM appointments").get(0).get(0);
    }

    /**
     * A function to return a list of strings from the customer class based on a provided lambda.
     * @param f Lambda for the string to be returned.
     * @return A list of strings.
     */
    public static ObservableList<String> getCustStr(CustomerFunction.Str f) {
        ObservableList<String> l = FXCollections.observableArrayList();

        cust.forEach(c -> l.add(f.strFn(c)));

        return l;
    }

    public static int getCustSize() {
        return cust.size();
    }

    /**
     * @return the master list of contacts
     */
    public static ObservableList<Contact> getContact_list() {
        return contact_list;
    }

    /**
     * Resets the list of appointments from a fresh query.
     * @return a copy of the appointment list.
     * @throws SQLException if there is an error with the query.
     */
    public static ObservableList<Appointment> refreshApptList() throws SQLException {
        appt.clear();
        ObservableList<Appointment> obs = FXCollections.observableArrayList();
        ArrayList<ArrayList<Object>> appl = get_all("SELECT * FROM appointments");
        for (ArrayList<Object> a : appl) {
            try {
                Appointment app = listToAppt(a);


                Appointment ap = new Appointment(
                        app.getAppointmentID(),
                        app.getTitle(),
                        app.getDesc(),
                        app.getLocation(),
                        app.getType(),
                        OffsetDateTime.of(app.getStart(), ZoneOffset.UTC).atZoneSameInstant(ZoneId.systemDefault().getRules().getOffset(Instant.now())).toLocalDateTime(),
                        OffsetDateTime.of(app.getEnd(), ZoneOffset.UTC).atZoneSameInstant(ZoneId.systemDefault().getRules().getOffset(Instant.now())).toLocalDateTime(),
                        app.getCreateDate().atOffset(ZoneId.systemDefault().getRules().getOffset(LocalDateTime.now())).toLocalDateTime(),
                        app.getCreatedBy(),
                        Timestamp.valueOf(OffsetDateTime.of(app.getLastUpdate().toLocalDateTime(), ZoneOffset.UTC).atZoneSameInstant(ZoneId.systemDefault().getRules().getOffset(Instant.now())).toLocalDateTime()),
                        app.getLastUpdatedBy(),
                        app.getCustomerID(),
                        app.getUserID(),
                        app.getContactID()
                        );

                obs.add(ap);
                appt.add(ap);


            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return obs;
    }

    /**
     *
     * Resets the list of customers from a fresh query.
     * @return a copy of the customer list.
     * @throws SQLException if there is an error with the query.
     */
    public static ObservableList<Customer> refreshCustList() throws SQLException {
        cust.clear();
        ArrayList<ArrayList<Object>> cuul = get_all("SELECT * FROM customers");
        ObservableList<Customer> l = FXCollections.observableArrayList();
        for (ArrayList<Object> c : cuul) {
                Customer cc = listToCust(c);
                Customer cus = new Customer(
                        cc.getCustomerID(),
                        cc.getCustomerName(),
                        cc.getAddress(),
                        cc.getPostalCode(),
                        cc.getPhone(),
                        OffsetDateTime.of(cc.getCreateDate(), ZoneOffset.UTC).atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime(),
                        cc.getCreatedBy(),
                        Timestamp.valueOf(cc.getLastUpdate().toLocalDateTime().atOffset(ZoneId.systemDefault().getRules().getOffset(LocalDateTime.now())).toLocalDateTime()),
                        cc.getLastUpdatedBy(),
                        cc.getDivisionID()
                        );
                l.add(cus);
                cust.add(cus);
        }
        return l;
    }

    /**
     * Queries the database and fills the list with countries.
     * @throws SQLException if there is an error querying the database.
     */
    public static void initCountryList() throws SQLException {
        ArrayList<ArrayList<Object>> coul = get_all("SELECT * FROM countries");
        for (ArrayList<Object> c : coul) {
                Country app = listToCountry(c);
                country_list.add(app);
        }
    }

    /**
     * Queries the database and fills the list with Contacts.
     * @throws SQLException if there is an error querying the database.
     */
    public static void initContactList() throws SQLException {
        ArrayList<ArrayList<Object>> conl = get_all("SELECT * FROM contacts");
        for (ArrayList<Object> c : conl) {
                Contact app = listToContact(c);
                contact_list.add(app);
        }
    }

    /**
     * @return the list of countries.
     */
    public static ObservableList<Country> getCounrtyList() {
        return country_list;
    }

    /**
     * a function to remove a specific customer from the database.
     * @param c the customer to remove.
     * @throws SQLException if there is an error querying the database.
     */
    public static void remove(Customer c) throws SQLException {
        st.execute("DELETE FROM customers WHERE Customer_ID = " + c.getCustomerID());
        refreshCustList();
    }

    /**
     * a function to remove a specific appointment from the database.
     * @param a the appointment to remove.
     * @throws SQLException if there is an error querying the database.
     */
    public static void remove(Appointment a) throws SQLException {
        st.execute("DELETE FROM appointments WHERE Appointment_ID = " + a.getAppointmentID());
        refreshApptList();
    }

    /**
     * a function to save a specific appointment from the database.
     * @param a the appointment to save.
     * @throws SQLException if there is an error querying the database.
     */
    public static void save(Appointment a) throws SQLException {

        if (appt.stream().noneMatch(aa -> aa.getAppointmentID() == a.getAppointmentID())) {

            st.execute("INSERT INTO appointments " +
                    "VALUES (" + a.getAppointmentID() + ", '" + a.getTitle() + "', '" + a.getDesc() + "', '" + a.getLocation() + "', '" +
                    a.getType() + "', '" + a.getStart() + "', '" + a.getEnd() + "', '" + a.getCreateDate() + "', '" + a.getCreatedBy() + "', '" +
                    a.getLastUpdate() + "', '" + a.getLastUpdatedBy() + "', " + a.getCustomerID() + ", " + a.getUserID() + ", " + a.getContactID() + ")");
        } else {
            String sql = "UPDATE appointments " +
                    "SET" +
                    " Title = '" + a.getTitle() +
                    "' , Description = '" + a.getDesc() +
                    "' , Location = '" + a.getLocation() +
                    "' , Type = '" + a.getType() +
                    "' , Start = '" + a.getStart() +
                    "' , End = '" + a.getEnd() +
                    "' , Create_Date = '" + a.getCreateDate() +
                    "' , Created_By = '" + a.getCreatedBy() +
                    "' , Last_Update = '" + a.getLastUpdate() +
                    "' , Last_Updated_By = '" + a.getLastUpdatedBy() +
                    "' , Customer_ID = " + a.getCustomerID() +
                    " , User_ID = " + a.getUserID() +
                    " , Contact_ID = " + a.getContactID() +
                    " WHERE Appointment_ID = " + a.appointmentID;
            st.execute(sql);
        }
        refreshApptList();
    }

    /**
     * a function to save a specific customer from the database.
     * @param c the customer to save.
     * @throws SQLException if there is an error querying the database.
     */
    public static void save(Customer c) throws SQLException {

        if (cust.stream().noneMatch(cc -> c.getCustomerID() == cc.getCustomerID())) {
            st.execute("INSERT INTO customers " +
                    "VALUES (" + c.getCustomerID() + ", '" + c.getCustomerName() + "', '" + c.getAddress() + "', '" + c.getPostalCode() + "', '" +
                    c.getPhone() + "', '" + c.getCreateDate() + "', '" + c.getCreatedBy() + "', '" + c.getLastUpdate() + "', '" + c.getLastUpdatedBy() + "', " +
                    c.getDivisionID() + ")");
        } else {
            st.execute("UPDATE customers SET" +
                    " Customer_Name = '" + c.getCustomerName() +
                    "' , Address = '" + c.getAddress() +
                    "' , Postal_Code = '" + c.getPostalCode() +
                    "' , Phone = '" + c.getPhone() +
                    "' , Create_Date = '" + c.getCreateDate() +
                    "' , Created_By = '" + c.getCreatedBy() +
                    "' , Last_Update = '" + c.getLastUpdate() +
                    "' , Last_Updated_By = '" + c.getLastUpdatedBy() +
                    "' , Division_ID = " + c.getDivisionID() +
                    " WHERE Customer_ID = " + c.getCustomerID());
        }
        refreshCustList();
    }

    /**
     * a function to match the customer to an id.
     * @param id The id to match.
     * @return The customer whose id matches the parameter
     * @throws DoesNotExistException if the customer does not exist.
     */
    public static Customer matchCustomer(int id) throws DoesNotExistException {

        List<Customer> lst = cust.stream().filter(c -> c.getCustomerID() == id).toList();

        if (lst.size() != 1) {
            throw new DoesNotExistException("Customer with id " + id + " does not exist");
        }

        return lst.get(0);
    }

    /**
     * a function to match the customer to a name.
     * @param name The name to match.
     * @return The customer whose name matches the parameter
     * @throws DoesNotExistException if the customer does not exist.
     */
    public static Customer matchCustomer(String name) throws DoesNotExistException {
        List<Customer> lst = cust.stream().filter(c -> c.getCustomerName().equals(name)).toList();

        if (lst.size() == 0) {
            throw new DoesNotExistException("Customer with name " + name + " does not exist");
        }

        return lst.get(0);
    }

    /**
     * a function to match the Contact to a name.
     * @param name The name to match.
     * @return The contact whose name matches the parameter
     * @throws DoesNotExistException if the contact does not exist.
     */
    public static Contact matchContact(String name) throws DoesNotExistException {
        List<Contact> lst = contact_list.stream().filter(c -> c.contactName().equals(name)).toList();

        if (lst.size() == 0) {
            throw new DoesNotExistException("Contact with id " + name + " does not exist");
        }

        return lst.get(0);
    }

    /**
     * a function to match the Contact to an id.
     * @param id The id to match.
     * @return The contact whose id matches the parameter
     * @throws DoesNotExistException if the contact does not exist.
     */
    public static Contact matchContact(int id) throws DoesNotExistException {
        List<Contact> lst = contact_list.stream().filter(c -> c.contactID() == id).toList();

        if (lst.size() == 0) {
            throw new DoesNotExistException("Contact with id " + id + " does not exist");
        }

        return lst.get(0);
    }

    /**
     * A function to take an ArrayList and return the country object for that list.
     * @param ca the arraylist with country object data.
     * @return The country object.
     * @throws IndexOutOfBoundsException if the ArrayList provided is the incorrect size.
     */
    public static Country listToCountry(ArrayList<Object> ca) throws IndexOutOfBoundsException {
        return new Country((Integer) ca.get(0), (String) ca.get(1), (LocalDateTime) ca.get(2), (String) ca.get(3), (Timestamp) ca.get(4),
                (String) ca.get(5));

    }

    /**
     * A function to take an ArrayList and return the contact object for that list.
     * @param ca the arraylist with contact object data.
     * @return The contact object.
     * @throws IndexOutOfBoundsException if the ArrayList provided is the incorrect size.
     */
    public static Contact listToContact(ArrayList<Object> ca) throws IndexOutOfBoundsException {

        return new Contact((Integer) ca.get(0), (String) ca.get(1), (String) ca.get(2));

    }

    /**
     * A function to take an ArrayList and return the customer object for that list.
     * @param ca the arraylist with customer object data.
     * @return The customer object.
     * @throws IndexOutOfBoundsException if the ArrayList provided is the incorrect size.
     */
    public static Customer listToCust(ArrayList<Object> ca) throws IndexOutOfBoundsException {
        return new Customer((Integer) ca.get(0), (String) ca.get(1), (String) ca.get(2), (String) ca.get(3), (String) ca.get(4),
                (LocalDateTime) ca.get(5), (String) ca.get(6), (Timestamp) ca.get(7), (String) ca.get(8), (Integer) ca.get(9));
    }

    /**
     * A function to take an ArrayList and return the appointment object for that list.
     * @param ca the arraylist with appointment object data.
     * @return The Appointment object.
     * @throws IndexOutOfBoundsException if the ArrayList provided is the incorrect size.
     */
    public static Appointment listToAppt(ArrayList<Object> ca) throws SQLException {
        try {
            return new Appointment((Integer) ca.get(0), (String) ca.get(1), (String) ca.get(2), (String) ca.get(3), (String) ca.get(4),
                    (LocalDateTime) ca.get(5), (LocalDateTime) ca.get(6), (LocalDateTime) ca.get(7), (String) ca.get(8), (Timestamp) ca.get(9),
                    (String) ca.get(10), (Integer) ca.get(11), (Integer) ca.get(12), (Integer) ca.get(13));
        } catch (IndexOutOfBoundsException e) {
            throw new SQLException(e);
        }
    }

    /**
     * converts the first level division id to a name.
     * @param id the id.
     * @return the division name.
     * @throws SQLException if there is an issue querying the database.
     * @throws IndexOutOfBoundsException if the id is not a match.
     */
    public static String getFirst_level_division(int id) throws SQLException, IndexOutOfBoundsException {
        ArrayList<ArrayList<Object>> l = get_all("SELECT Division FROM first_level_divisions WHERE Division_ID = " + id);
        return (String) l.get(0).get(0);

    }

    /**
     * converts the first level division name to an id.
     * @param name the name.
     * @return the division id.
     * @throws SQLException if there is an issue querying the database.
     * @throws IndexOutOfBoundsException if the name is not a match.
     */
    public static int getFirst_level_division(String name) throws SQLException, IndexOutOfBoundsException {
        ArrayList<ArrayList<Object>> l = get_all("SELECT Division_ID FROM first_level_divisions WHERE Division = '" + name + "'");
        return (int) l.get(0).get(0);

    }

    /**
     * @param country_id the name.
     * @return the division names in the country.
     * @throws SQLException if there is an issue querying the database.
     */
    public static ArrayList<String> getDivs(int country_id) throws SQLException {

        ArrayList<ArrayList<Object>> a = get_all("SELECT Division FROM first_level_divisions WHERE Country_ID = " + country_id);

        ArrayList<String> s = new ArrayList<>();
        for (ArrayList<Object> r : a) {
            s.add((String) r.get(0));
        }

        return s;
    }

    /**
     *
     * @param div_id the id of the division.
     * @return the country whose home to the division.
     * @throws SQLException if there is an error querying the database.
     */
    public static Country country_from_div(int div_id) throws SQLException {
        ArrayList<ArrayList<Object>> l = get_all("SELECT Country_ID FROM first_level_divisions WHERE Division_ID =" + div_id);
        return country_list.stream().filter(ccc -> ccc.countryID() == (int) l.get(0).get(0)).toList().get(0);
    }

    /**
     * @param name the contact name.
     * @return a list of appointments associated with the contact.
     */
    public static ObservableList<Appointment> getApptsFromContact(String name) {
        Contact cc = contact_list.stream().filter(c -> c.contactName().equals(name)).toList().get(0);
        return appt.stream().filter(a -> cc.contactID() == a.getContactID()).collect(Collectors.toCollection(FXCollections::observableArrayList));
    }

    /**
     *
     * @param c the customer.
     * @return true if the customer has no appointments associated with it.
     */
    public static boolean customerAppointments(Customer c) {
        return appt.stream().noneMatch(a -> c.getCustomerID() == a.getCustomerID());
    }

    /**
     * Queryies the database from a string, returns an array of an array where the inner array is the columns.
     * @param q a database query.
     * @return an ArrayList Of an ArrayList of objects. This array is the table that was queried.
     * @throws SQLException if there is an error querying the database or if the query q is bad.
     */
    static ArrayList<ArrayList<Object>> get_all(String q) throws SQLException {
        ArrayList<ArrayList<Object>> rows = new ArrayList<>();
        ArrayList<String> cols = new ArrayList<>();
        ResultSet r = st.executeQuery(q);
        ResultSetMetaData rm = r.getMetaData();
        int colc = rm.getColumnCount();
        for (int i = 1; i <= colc; i++) {
            cols.add(rm.getColumnName(i));
        }
        while (r.next()) {
            ArrayList<Object> row = new ArrayList<>();
            for (String colnm : cols) {
                Object o = r.getObject(colnm);
                row.add(o);
            }
            rows.add(row);
        }
        return rows;
    }
}
