package software.DMS;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import software.DMS.Database.LoginFailedException;

import java.io.File;
import java.io.IOException;
import java.sql.*;

import java.time.*;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * @author Daniel Foster
 * The lambda functions are defined in the DataBase class located in the DataBase file.
 * There are many uses of the lambda functions, and they both greatly reduce the ammount
 * and complexity of the code written, whilst inproving readability.
 * The custom report is a report of total customers by country. This is dynamic if more countries were to be added in the future.
 * The main class where everything is brought together to make something special.
 */
public class DMS extends Application {
    private static String CURRENT_USER;
    private static Stage parent;
    static int OPEN = 0;
    static OffsetTime OFFICE_OPEN = OffsetTime.of(8, 0, 0, 0, ZoneOffset.of(String.valueOf(ZoneId.of("America/New_York").getRules().getOffset(Instant.now()))));
    static OffsetTime OFFICE_CLOSE = OffsetTime.of(22, 0, 0, 0, ZoneOffset.of(String.valueOf(ZoneId.of("America/New_York").getRules().getOffset(Instant.now()))));
    static ZoneId TIME_ZONE = ZoneId.systemDefault();
    private static final ResourceBundle bun = ResourceBundle.getBundle("lang", Locale.getDefault());

    /**
     * This class holds the main landing window for logging in, as well as a function to log out.
     */
    public static class login {
        private static final StackPane root = new StackPane();
        private static final Scene scene = new Scene(root, 400, 400);
        private static final TextField pass = new TextField();
        private static final TextField user = new TextField();
        private static final Label userl = new Label(bun.getString("Username") + ":");
        private static final Label passl = new Label(bun.getString("Password") + ":");
        private static final Label loc = new Label(TIME_ZONE.getId());
        private static final Alert err = new Alert(Alert.AlertType.ERROR, "Username or password is incorrect", ButtonType.CLOSE);
        private static final Alert inf = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.OK);

        /**
         * A function that sets what the buttons and text boxes do.
         * @lambda There are two uses of lambda the first is used to set the actions for the text boxes to log in.
         * @lambda  The other is used to filter appointments between now and 15 minutes from now.
         */
        public static void init() {
            err.setTitle(bun.getString("Login-Failed"));
            err.setHeaderText(bun.getString("Login-Failed"));
            user.setMaxWidth(150);
            user.setOnAction(e -> {
                try {
                    Database.login(user.getText(), pass.getText());
                    if (OPEN == 0) {
                        Database.initContactList();
                        Database.initCountryList();
                        customerWin.init();
                        tables.init();
                        appointmentWin.init();
                        tables.updateApptTableData();
                        OPEN++;
                    } else {
                        tables.updateApptTableData();
                        tables.updateCustTableData();
                    }
                    tables.display();
                    CURRENT_USER = user.getText();
                    user.setText("");
                    pass.setText("");

                    File f = new File("login_activity.txt");
                    LogToFile.save(CURRENT_USER, f);

                    ObservableList<Appointment> appts = Database.refreshApptList().stream().filter(aa -> aa.getStart().isAfter(LocalDateTime.now()) && aa.getStart().isBefore(LocalDateTime.now())).collect(Collectors.toCollection(FXCollections::observableArrayList));
                    if (appts.size() > 0) {
                        Appointment appt = appts.get(0);
                        inf.setContentText("You Have an appointment(" + appt.getAppointmentID() + ") soon at " + appt.getStart().getHour() + ":" + appt.getStart().getMinute() + " on " + appt.getStart().toLocalDate());
                        inf.showAndWait();
                    } else {
                        inf.setContentText("You have no appointments soon");
                        inf.showAndWait();
                    }
                } catch (SQLException ex) {
                    File f = new File("login_activity.txt");
                    try {
                        LogToFile.save(LocalDateTime.now() + " " + user.getText() + " " + "Fail(Database Connection Error)", f);
                    } catch (IOException exc) {
                        err.setContentText(bun.getString("Cannot write to file login_activity.txt"));
                        err.showAndWait();
                    }
                    err.setContentText(bun.getString("Database-con-err"));
                    err.showAndWait();
                } catch (IOException ex) {
                    err.setContentText(bun.getString("Cannot write to file login_activity.txt"));
                    err.showAndWait();
                } catch (LoginFailedException ex) {
                    File f = new File("login_activity.txt");
                    try {
                        LogToFile.save(LocalDateTime.now() + " " + user.getText() + " " + "Fail(Incorrect username or password)", f);
                    } catch (IOException exc) {
                        err.setContentText(bun.getString("Cannot write to file login_activity.txt"));
                        err.showAndWait();
                    }
                    err.setContentText(bun.getString("Incorrect-Username"));
                    err.showAndWait();
                }
            });

            StackPane.setMargin(userl, new Insets(0, 0, 50, 0));

            pass.setMaxWidth(150);
            pass.setOnAction(e -> {
                try {
                    Database.login(user.getText(), pass.getText());
                    if (OPEN == 0) {
                        Database.initContactList();
                        Database.initCountryList();
                        customerWin.init();
                        tables.init();
                        appointmentWin.init();
                        tables.updateApptTableData();
                        OPEN++;
                    } else {
                        tables.updateApptTableData();
                        tables.updateCustTableData();
                    }
                    tables.display();
                    CURRENT_USER = user.getText();
                    user.setText("");
                    pass.setText("");

                    File f = new File("login_activity.txt");
                    LogToFile.save(LocalDateTime.now() + " " + CURRENT_USER + " " + "Success", f);

                    ObservableList<Appointment> appts = Database.refreshApptList().stream().filter(aa -> aa.getStart().isAfter(LocalDateTime.now()) && aa.getStart().isBefore(LocalDateTime.now())).collect(Collectors.toCollection(FXCollections::observableArrayList));
                    if (appts.size() > 0) {
                        Appointment appt = appts.get(0);
                        inf.setContentText("You Have an appointment(" + appt.getAppointmentID() + ") soon at " + appt.getStart().getHour() + ":" + appt.getStart().getMinute() + " on " + appt.getStart().toLocalDate());
                        inf.showAndWait();
                    } else {
                        inf.setContentText("You have no appointments soon");
                        inf.showAndWait();
                    }
                } catch (SQLException ex) {
                    File f = new File("login_activity.txt");
                    try {
                        LogToFile.save(LocalDateTime.now() + " " + user.getText() + " " + "Fail(Database Connection Error)", f);
                    } catch (IOException exc) {
                        err.setContentText(bun.getString("Cannot write to file login_activity.txt"));
                        err.showAndWait();
                    }
                    err.setContentText(bun.getString("Database-con-err"));
                    err.showAndWait();
                } catch (IOException ex) {
                    err.setContentText(bun.getString("Cannot write to file login_activity.txt"));
                    err.showAndWait();
                } catch (LoginFailedException ex) {
                    File f = new File("login_activity.txt");
                    try {
                        LogToFile.save(LocalDateTime.now() + " " + user.getText() + " " + "Fail(Incorrect username or password)", f);
                    } catch (IOException exc) {
                        err.setContentText(bun.getString("Cannot write to file login_activity.txt"));
                        err.showAndWait();
                    }
                    err.setContentText(bun.getString("Incorrect-Username"));
                    err.showAndWait();
                }
            });

            StackPane.setMargin(pass, new Insets(150, 0, 0, 0));

            StackPane.setMargin(passl, new Insets(100, 0, 0, 0));

            StackPane.setAlignment(loc, Pos.BOTTOM_RIGHT);

            parent.setTitle("DMS-1");

            root.getChildren().addAll(user, userl, pass, passl, loc);
        }

        /**
         * Displays the login window.
         */
        public static void display() {
            parent.setScene(scene);
            parent.centerOnScreen();
        }
        public static void logoff() {
            tables.logoff();
            login.display();
        }
    }

    /**
     * Holds all the necessary functions and variables for Adding and modifying the customer table.
     */
    public static class customerWin {
        private static final StackPane root = new StackPane();
        private static final Scene scene = new Scene(root, 750, 400);
        private static final GridPane g = new GridPane();
        private static final TextField name = new TextField();
        private static final TextField address = new TextField();
        private static final TextField postal = new TextField();
        private static final TextField phone = new TextField();
        private static final ComboBox<String> country = new ComboBox<>();
        private static final ComboBox<String> state = new ComboBox<>();
        private static final Label namel = new Label("Name");
        private static final Label addrl = new Label("Address");
        private static final Label postl = new Label("Postal Code");
        private static final Label phonel = new Label("Phone");
        private static final Label countryl = new Label("Country");
        private static final Label statel = new Label("State");
        private static final Label topl = new Label();
        private static final Button cancel = new Button("Cancel");
        private static final Button save = new Button("Save");
        private static final Insets text = new Insets(25, 50, 25, 50);
        private static final ObservableList<String> states = FXCollections.observableArrayList();
        private static final Alert conf = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.APPLY, ButtonType.CANCEL);
        private static final Alert err = new Alert(Alert.AlertType.ERROR, "", ButtonType.CLOSE);
        private static final Alert inf = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.OK);

        /**
         * Sets all the common elements of the modification and add windows for the customer table.
         * @lambda The only uses are to set button actions.
         */
        public static void init() {
            state.setPrefWidth(150);
            country.setPrefWidth(150);

            topl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 15));
            namel.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            addrl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            phonel.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            postl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            countryl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            statel.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));

            conf.setTitle("Save?");
            err.setTitle(bun.getString("Error"));
            inf.setTitle("Confirmed");

            ObservableList<Country> cq = Database.getCounrtyList();
            ObservableList<String> sl = FXCollections.observableArrayList();
            for (Country c : cq) {
                sl.add(c.country());
            }

            country.setItems(sl);

            country.setOnAction(e -> {
                try {
                    states.clear();
                    Country ddd = Database.getCounrtyList().stream().filter(co -> Objects.equals(co.country(), country.getSelectionModel().getSelectedItem())).toList().get(0);
                    ArrayList<String> lll = Database.getDivs(ddd.countryID());
                    states.addAll(lll);
                    state.getSelectionModel().select(0);
                } catch (SQLException ex) {
                    err.setContentText("An error has occured. Please try again later.");
                    err.showAndWait();
                    login.logoff();
                }
            });

            try {
                ArrayList<String> lll = Database.getDivs(1);
                states.addAll(lll);
            } catch (SQLException ex) {
                err.setContentText("An error has occured. Please try again later.");
                err.showAndWait();
                login.logoff();
            }

            state.setItems(states);

            GridPane.setRowIndex(name, 1);
            GridPane.setMargin(name, text);
            GridPane.setRowIndex(namel, 1);
            GridPane.setColumnIndex(name, 1);
            GridPane.setRowIndex(address, 2);
            GridPane.setMargin(address, text);
            GridPane.setRowIndex(addrl, 2);
            GridPane.setColumnIndex(address, 1);
            GridPane.setRowIndex(postal, 3);
            GridPane.setMargin(postal, text);
            GridPane.setRowIndex(postl, 3);
            GridPane.setColumnIndex(postal, 1);
            GridPane.setRowIndex(phone, 4);
            GridPane.setMargin(phone, text);
            GridPane.setRowIndex(phonel, 4);
            GridPane.setColumnIndex(phone, 1);
            GridPane.setRowIndex(country, 1);
            GridPane.setRowIndex(countryl, 1);
            GridPane.setRowIndex(state, 3);
            GridPane.setRowIndex(statel, 3);
            GridPane.setColumnIndex(country, 3);
            GridPane.setColumnIndex(countryl, 2);
            GridPane.setMargin(countryl, new Insets(0, 25, 0, 0));
            GridPane.setColumnIndex(state, 3);
            GridPane.setColumnIndex(statel, 2);
            GridPane.setRowIndex(cancel, 4);
            GridPane.setColumnIndex(cancel, 3);
            GridPane.setRowIndex(save, 4);
            GridPane.setColumnIndex(save, 3);

            GridPane.setMargin(save, new Insets(0, 0, 0, 75));


            StackPane.setMargin(g, new Insets(50));

            cancel.setOnAction(e -> {
                name.setText("");
                phone.setText("");
                address.setText("");
                postal.setText("");
                country.getSelectionModel().select(0);
                state.getSelectionModel().select(0);
                tables.display();
            });

            name.textProperty().addListener(((observableValue, s, t1) -> {
                if(t1.length() > 50) {
                    name.setText(s);
                }
            }));

            address.textProperty().addListener(((observableValue, s, t1) -> {
                if(t1.length() > 100) {
                    address.setText(s);
                }
            }));

            postal.textProperty().addListener(((observableValue, s, t1) -> {
                if(t1.length() > 50) {
                    postal.setText(s);
                }
            }));

            country.getSelectionModel().select(0);
            state.getSelectionModel().select(0);

            g.getChildren().addAll(topl, name, namel, address, addrl, postal, postl, phone, phonel, country, countryl, state, statel, save, cancel);
            root.getChildren().add(g);
        }

        /**
         * Displays the Add window for the customer table.
         * @lambda the only use of lambda is to set button actions.
         */
        public static void displayAdd() {

            topl.setText("Add Customer");
            save.setOnAction(e -> {
                try {
                    if (name.getText().equals("") || address.getText().equals("") || postal.getText().equals("") || phone.getText().equals("")) {
                        err.setContentText(bun.getString("Missing"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }


                    if (!Pattern.compile("\\d+\\s\\w+").matcher(address.getText()).find()) {
                        err.setContentText(bun.getString("Address"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }


                    if (postal.getText().length() != 6) {
                        err.setContentText(bun.getString("Postal"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }


                    if (!Pattern.compile("\\d{3}-\\d{3}-\\d{4}").matcher(phone.getText()).find() && !Pattern.compile("\\d{2}-\\d{3}-\\d{3}-\\d{4}").matcher(phone.getText()).find() && !Pattern.compile("\\d-\\d{3}-\\d{3}-\\d{4}").matcher(phone.getText()).find()) {
                        err.setContentText(bun.getString("Phone"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    conf.setContentText("Add Customer " + name.getText() + "?");
                    conf.showAndWait();

                    if (conf.getResult() != ButtonType.APPLY) {
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }
                    System.out.println(LocalDateTime.now());
                    int id = Database.getCustSize() + 1;
                    Customer c = new Customer(id, name.getText(), address.getText(), postal.getText(),
                            phone.getText(), OffsetDateTime.of(LocalDateTime.now(), ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).atZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime(), CURRENT_USER,
                            Timestamp.valueOf(OffsetDateTime.of(LocalDateTime.now(), ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).atZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime()), CURRENT_USER,
                            Database.getFirst_level_division(state.getSelectionModel().getSelectedItem()));
                    Database.save(c);
                    tables.updateCustTableData();
                    appointmentWin.updateCustomers();
                    inf.setContentText("Customer has been saved");
                    inf.showAndWait();
                    name.setText("");
                    phone.setText("");
                    address.setText("");
                    postal.setText("");
                    tables.display();

                } catch (SQLException ex) {
                    err.setContentText(bun.getString("Other-Error"));
                    err.showAndWait();
                }
            });
            parent.setScene(scene);
            parent.centerOnScreen();
        }

        /**
         * Displays the modification window for the customer table.
         * @param c The customer to be modified.
         * @throws SQLException if there is a database error.
         * @lambda The only use of lambda is to set button actions.
         */
        public static void displayMod(Customer c) throws SQLException {
            topl.setText("Mod Customer");
            name.setText(c.getCustomerName());
            address.setText(c.getAddress());
            postal.setText(c.getPostalCode());
            phone.setText(c.getPhone());

            String dname = Database.getFirst_level_division(c.getDivisionID());
            Country cuntry = Database.country_from_div(c.getDivisionID());
            country.getSelectionModel().select(cuntry.country());

            try {
                states.clear();
                int ccid = cuntry.countryID();
                ArrayList<String> stt = Database.getDivs(ccid);
                states.addAll(stt);
            } catch (SQLException ex) {
                err.setContentText("An error has occured. Please try again later.");
                err.showAndWait();
                login.logoff();
            }
            state.getSelectionModel().select(dname);


            save.setOnAction(e -> {
                try {
                    if (name.getText().equals("") || address.getText().equals("") || postal.getText().equals("") || phone.getText().equals("")) {
                        err.setContentText(bun.getString("Missing"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    if (!Pattern.compile("\\d+\\s\\w+").matcher(address.getText()).find()) {
                        err.setContentText(bun.getString("Address"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }


                    if (postal.getText().length() != 6) {
                        err.setContentText(bun.getString("Postal"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    if (!Pattern.compile("\\d{3}-\\d{3}-\\d{4}").matcher(phone.getText()).find() && !Pattern.compile("\\d{2}-\\d{3}-\\d{3}-\\d{4}").matcher(phone.getText()).find() && !Pattern.compile("\\d-\\d{3}-\\d{3}-\\d{4}").matcher(phone.getText()).find()) {
                        err.setContentText(bun.getString("Phone"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    conf.setContentText("Add Customer " + name.getText() + "?");
                    conf.showAndWait();

                    if (conf.getResult() != ButtonType.APPLY) {
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    Customer cc = new Customer(c.getCustomerID(), name.getText(), address.getText(), postal.getText(),
                            phone.getText(), OffsetDateTime.of(c.getCreateDate(), ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).atZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime(), c.getCreatedBy(),
                            Timestamp.valueOf(OffsetDateTime.of(LocalDateTime.now(), ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).atZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime()), CURRENT_USER,
                            Database.getFirst_level_division(state.getSelectionModel().getSelectedItem()));
                    Database.save(cc);
                    tables.updateCustTableData();
                    appointmentWin.updateCustomers();
                    inf.setContentText("Customer has been saved");
                    inf.showAndWait();
                    name.setText("");
                    phone.setText("");
                    address.setText("");
                    postal.setText("");
                    tables.display();

                } catch (SQLException ex) {
                    err.setContentText(bun.getString("Other-Error"));
                    err.showAndWait();
                }
            });
            parent.setScene(scene);
            parent.centerOnScreen();
        }
    }

    /**
     * Holds all the necessary functions and variables for adding and updating the appointment table.
     */
    public static class appointmentWin {
        private static final StackPane root = new StackPane();
        private static final Scene scene = new Scene(root, 750, 400);
        private static final GridPane g = new GridPane();
        private static final TextField title = new TextField();
        private static final TextField desc = new TextField();
        private static final TextField location = new TextField();
        private static final TextField type = new TextField();
        private static final ComboBox<String> customer = new ComboBox<>();
        private static final ComboBox<String> contact = new ComboBox<>();
        private static final Label topl = new Label();
        private static final Label titlel = new Label("Title");
        private static final Label descl = new Label("Description");
        private static final Label locationl = new Label("Location");
        private static final Label typel = new Label("Type");
        private static final Label contactl = new Label("Contact");
        private static final Label customerl = new Label("Customer");
        private static final Label startl = new Label("Start Date");
        private static final Label endl = new Label("End");
        private static final Button cancel = new Button("Cancel");
        private static final Button save = new Button("Save");
        private static final Insets text = new Insets(25, 50, 25, 50);
        private static ObservableList<String> customers = FXCollections.observableArrayList();
        private static final ObservableList<String> contacts = FXCollections.observableArrayList();
        private static final DatePicker startd = new DatePicker();
        private static final ComboBox<String> starth = new ComboBox<>();
        private static final ComboBox<String> startm = new ComboBox<>();
        private static final ComboBox<String> endh = new ComboBox<>();
        private static final ComboBox<String> endm = new ComboBox<>();
        private static final ObservableList<String> hour = FXCollections.observableArrayList();
        private static final ObservableList<String> min = FXCollections.observableArrayList();
        private static final Label stime = new Label("Start");
        private static final Label etime = new Label("End");
        private static final Alert conf = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.APPLY, ButtonType.CANCEL);
        private static final Alert err = new Alert(Alert.AlertType.ERROR, "", ButtonType.CLOSE);
        private static final Alert inf = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.OK);

        /**
         * A functions that updates the customer names to choose from when adding or modifying an appointment.
         * @lambda Custom lambda Used to get a list of customer names. Replaced a length for statement.
         */
        public static void updateCustomers() {
            customers.clear();

            /* the below is an example of the lambda function used for
               the customer list. replaced a for loop, makes for easier to
               read cleaner code.
               */

            customers = Database.getCustStr(Customer::getCustomerName);
            customer.setItems(customers);
            customer.getSelectionModel().select(0);
        }

        /**
         * Sets the common elements for the modification and add windows for the appointment table.
         * @lambda the only use of lambda is to set button actions.
         */
        public static void init() {

            updateCustomers();

            for(int i = 0; i <= 24; i++) {
                if (i < 10) {
                    hour.add("" + 0 + "" + i);
                } else {
                    hour.add(String.valueOf(i));
                }
            }
            for(int i = 0; i < 60; i++) {
                if (i < 10) {
                    min.add("" + 0 + "" + i);
                } else {
                    min.add(String.valueOf(i));
                }
            }

            startd.setPrefWidth(150);
            customer.setPrefWidth(150);
            contact.setPrefWidth(150);

            topl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 15));
            titlel.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            descl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            locationl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            typel.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            startl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            endl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            customerl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            contactl.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            stime.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
            etime.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));

            conf.setTitle("Save?");
            err.setTitle("Error");
            inf.setTitle("Confirmed");

            ObservableList<Contact> cl = Database.getContact_list();
            for (Contact c : cl) {
                contacts.add(c.contactName());

            }

            starth.setItems(hour);
            endh.setItems(hour);
            startm.setItems(min);
            endm.setItems(min);
            customer.setItems(customers);
            contact.setItems(contacts);

            GridPane.setRowIndex(title, 1);
            GridPane.setMargin(title, text);
            GridPane.setRowIndex(titlel, 1);
            GridPane.setColumnIndex(title, 1);
            GridPane.setRowIndex(desc, 2);
            GridPane.setMargin(desc, text);
            GridPane.setRowIndex(descl, 2);
            GridPane.setColumnIndex(desc, 1);
            GridPane.setRowIndex(location, 3);
            GridPane.setMargin(location, text);
            GridPane.setRowIndex(locationl, 3);
            GridPane.setColumnIndex(location, 1);
            GridPane.setRowIndex(type, 4);
            GridPane.setMargin(type, text);
            GridPane.setRowIndex(typel, 4);
            GridPane.setColumnIndex(type, 1);
            GridPane.setRowIndex(contact, 4);
            GridPane.setRowIndex(contactl, 4);
            GridPane.setRowIndex(customer, 3);
            GridPane.setRowIndex(customerl, 3);
            GridPane.setColumnIndex(contact, 3);
            GridPane.setColumnIndex(contactl, 2);
            GridPane.setMargin(contactl, new Insets(0, 25, 0, 0));
            GridPane.setColumnIndex(customer, 3);
            GridPane.setColumnIndex(customerl, 2);
            GridPane.setRowIndex(cancel, 5);
            GridPane.setColumnIndex(cancel, 3);
            GridPane.setRowIndex(save, 5);
            GridPane.setColumnIndex(save, 3);
            GridPane.setColumnIndex(startl, 2);
            GridPane.setRowIndex(startl, 1);
            GridPane.setColumnIndex(startd, 3);
            GridPane.setRowIndex(startd, 1);
            GridPane.setColumnIndex(starth, 3);
            GridPane.setRowIndex(starth, 2);
            GridPane.setMargin(starth, new Insets(0, 0, 40, 0));
            GridPane.setColumnIndex(startm, 3);
            GridPane.setRowIndex(startm, 2);
            GridPane.setMargin(startm, new Insets(0, 0, 40, 85));
            GridPane.setColumnIndex(endl, 2);
            GridPane.setRowIndex(endl, 2);
            GridPane.setColumnIndex(endh, 3);
            GridPane.setRowIndex(endh, 2);
            GridPane.setMargin(endh, new Insets(40, 0, 0, 0));
            GridPane.setColumnIndex(endm, 3);
            GridPane.setRowIndex(endm, 2);
            GridPane.setMargin(endm, new Insets(40, 0, 0, 85));
            GridPane.setColumnIndex(stime, 2);
            GridPane.setRowIndex(stime, 2);
            GridPane.setMargin(stime, new Insets(0, 0, 40, 0));
            GridPane.setColumnIndex(etime, 2);
            GridPane.setRowIndex(etime, 2);
            GridPane.setMargin(etime, new Insets(40, 0, 0, 0));

            GridPane.setMargin(save, new Insets(0, 0, 0, 75));


            StackPane.setMargin(g, new Insets(50));

            cancel.setOnAction(e -> {
                title.setText("");
                desc.setText("");
                type.setText("");
                location.setText("");
                startd.setValue(LocalDate.now());
                startm.getSelectionModel().select(0);
                starth.getSelectionModel().select(0);
                endm.getSelectionModel().select(0);
                endh.getSelectionModel().select(0);
                customer.getSelectionModel().select(0);
                contact.getSelectionModel().select(0);
                tables.display();
            });

            title.textProperty().addListener(((observableValue, s, t1) -> {
                if(t1.length() > 50) {
                    title.setText(s);
                }
            }));

            desc.textProperty().addListener(((observableValue, s, t1) -> {
                if(t1.length() > 50) {
                    desc.setText(s);
                }
            }));

            location.textProperty().addListener(((observableValue, s, t1) -> {
                if(t1.length() > 50) {
                    location.setText(s);
                }
            }));

            type.textProperty().addListener(((observableValue, s, t1) -> {
                if(t1.length() > 50) {
                    type.setText(s);
                }
            }));

            startd.setValue(LocalDate.now());
            startm.getSelectionModel().select(0);
            starth.getSelectionModel().select(0);
            endm.getSelectionModel().select(0);
            endh.getSelectionModel().select(0);
            customer.getSelectionModel().select(0);
            contact.getSelectionModel().select(0);

            g.getChildren().addAll(topl, title, titlel, desc, descl, location, locationl, type, typel, startd, startl, customer, customerl, contact, contactl, save, cancel, startm, starth, endm, endh);
            root.getChildren().add(g);
        }

        /**
         * Displays the add window for the appointment table.
         * @lambda the only use of lambda is to set button actions.
         */
        public static void displayAdd() {

            topl.setText("Add Appointment");
            save.setOnAction(e -> {
                OffsetDateTime zoned_start = OffsetDateTime.of(startd.getValue(), LocalTime.of(Integer.parseInt(starth.getSelectionModel().getSelectedItem()), Integer.parseInt(startm.getSelectionModel().getSelectedItem())), TIME_ZONE.getRules().getOffset(Instant.now()));
                OffsetDateTime zoned_end = OffsetDateTime.of(startd.getValue(), LocalTime.of(Integer.parseInt(endh.getSelectionModel().getSelectedItem()), Integer.parseInt(endm.getSelectionModel().getSelectedItem())), TIME_ZONE.getRules().getOffset(Instant.now()));


                OffsetDateTime utc_end = zoned_end.toInstant().atOffset(ZoneOffset.UTC);
                OffsetDateTime utc_start = zoned_start.toInstant().atOffset(ZoneOffset.UTC);

                OffsetDateTime est_start = zoned_start.toInstant().atOffset(ZoneOffset.UTC);

                try {
                    if (title.getText().equals("") || desc.getText().equals("") || type.getText().equals("") || startd.getConverter().toString().equals("")) {
                        err.setContentText(bun.getString("Missing"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    if (utc_start.toLocalTime().isAfter(utc_end.toLocalTime())) {
                        err.setContentText(bun.getString("Start"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    if (OFFICE_OPEN.isAfter(est_start.toOffsetTime()) || OFFICE_CLOSE.isBefore(est_start.toOffsetTime())) {
                        err.setContentText(bun.getString("Office"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    conf.setContentText("Add Aappointment " + title.getText() + "?");
                    conf.showAndWait();

                    if (conf.getResult() != ButtonType.APPLY) {
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    for (Appointment appt : Database.refreshApptList()) {
                        if (!((zoned_start.toLocalDateTime().isBefore(appt.getStart()) && (zoned_end.toLocalDateTime().isBefore(appt.getEnd())))
                                || ((zoned_start.toLocalDateTime().isAfter(appt.getStart())) && (zoned_end.toLocalDateTime().isAfter(appt.getEnd()))))) {
                            err.setContentText(bun.getString("Overlapping"));
                            err.showAndWait();
                            return;
                                }
                    }

                    int id = Integer.parseInt(String.valueOf(Database.getApptId() + 1));
                    int cid = Database.matchCustomer(customer.getSelectionModel().getSelectedItem()).getCustomerID();
                    int ccid = Database.matchContact(contact.getSelectionModel().getSelectedItem()).contactID();
                    Appointment aa = new Appointment(id, title.getText(), desc.getText(), location.getText(),
                            type.getText(), OffsetDateTime.of(startd.getValue(), LocalTime.of(Integer.parseInt(starth.getSelectionModel().getSelectedItem()), Integer.parseInt(startm.getSelectionModel().getSelectedItem())), ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).atZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime(),
                            OffsetDateTime.of(startd.getValue(), LocalTime.of(Integer.parseInt(endh.getSelectionModel().getSelectedItem()), Integer.parseInt(endm.getSelectionModel().getSelectedItem())), ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).atZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime(), LocalDateTime.now(ZoneId.of("UTC")), CURRENT_USER,
                            Timestamp.valueOf(LocalDateTime.now(ZoneId.of("UTC"))), CURRENT_USER,
                            cid, 1, ccid);
                    Database.save(aa);
                    tables.updateApptTableData();
                    inf.setContentText("Appointment has been saved");
                    inf.showAndWait();
                    title.setText("");
                    desc.setText("");
                    type.setText("");
                    location.setText("");
                    startd.setValue(LocalDate.now());
                    startm.getSelectionModel().select(0);
                    starth.getSelectionModel().select(0);
                    endm.getSelectionModel().select(0);
                    endh.getSelectionModel().select(0);
                    customer.getSelectionModel().select(0);
                    contact.getSelectionModel().select(0);
                    tables.display();

                } catch (SQLException ex) {
                    err.setContentText(bun.getString("Other-Error"));
                    err.showAndWait();
                } catch (Database.DoesNotExistException ex) {
                    err.setContentText("An error has occured. Please try again later.");
                    err.showAndWait();
                    login.logoff();
                }
            });
            parent.setScene(scene);
            parent.centerOnScreen();
        }

        /**
         * Displays the modification window for the appointment table.
         * @param a the appointment to modify.
         * @throws SQLException if there is a database error.
         * @throws Database.DoesNotExistException if there is an error matching the contact and customer to the provided appointment.
         * @lambda the only use of lambda is to set button actions.
         */
        public static void displayMod(Appointment a) throws SQLException, Database.DoesNotExistException {

            OffsetDateTime start_datee = OffsetDateTime.of(a.getStart().toLocalDate(), LocalTime.of(a.getStart().getHour(), a.getEnd().getMinute()), ZoneOffset.UTC);
            OffsetDateTime end_datee = OffsetDateTime.of(a.getStart().toLocalDate(), LocalTime.of(a.getEnd().getHour(), a.getEnd().getMinute()), ZoneOffset.UTC);

            OffsetDateTime start_date = start_datee.toInstant().atOffset(TIME_ZONE.getRules().getOffset(Instant.now()));
            OffsetDateTime end_date = end_datee.toInstant().atOffset(TIME_ZONE.getRules().getOffset(Instant.now()));

            title.setText(a.getTitle());
            desc.setText(a.getDesc());
            location.setText(a.getLocation());
            type.setText(a.getType());
            startd.setValue(a.getStart().toLocalDate());
            starth.getSelectionModel().select(a.getStart().getHour());
            startm.getSelectionModel().select(a.getStart().getMinute());
            endh.getSelectionModel().select(a.getEnd().getHour());
            endm.getSelectionModel().select(a.getEnd().getMinute());
            contact.getSelectionModel().select(Database.matchContact(a.getContactID()).contactName());
            customer.getSelectionModel().select(Database.matchCustomer(a.getCustomerID()).getCustomerName());
            topl.setText("Modify Appointment");
            System.out.println(a.getStart());
            save.setOnAction(e -> {

                OffsetDateTime zoned_start = OffsetDateTime.of(startd.getValue(), LocalTime.of(Integer.parseInt(starth.getSelectionModel().getSelectedItem()), Integer.parseInt(startm.getSelectionModel().getSelectedItem())), TIME_ZONE.getRules().getOffset(Instant.now()));
                OffsetDateTime zoned_end = OffsetDateTime.of(startd.getValue(), LocalTime.of(Integer.parseInt(endh.getSelectionModel().getSelectedItem()), Integer.parseInt(endm.getSelectionModel().getSelectedItem())), TIME_ZONE.getRules().getOffset(Instant.now()));

                OffsetDateTime utc_end = zoned_end.toInstant().atOffset(ZoneOffset.UTC);
                OffsetDateTime utc_start = zoned_start.toInstant().atOffset(ZoneOffset.UTC);

                OffsetDateTime est_start = zoned_start.toInstant().atOffset(ZoneOffset.UTC);

                try {
                    if (title.getText().equals("") || desc.getText().equals("") || type.getText().equals("") || startd.getConverter().toString().equals("")) {
                        err.setContentText(bun.getString("Missing"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    if (utc_start.toLocalTime().isAfter(utc_end.toLocalTime())) {
                        err.setContentText(bun.getString("Start"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    if (OFFICE_OPEN.isAfter(est_start.toOffsetTime()) || OFFICE_CLOSE.isBefore(est_start.toOffsetTime())) {
                        err.setContentText(bun.getString("Office"));
                        err.showAndWait();
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    conf.setContentText("Save Appointment " + title.getText() + "?");
                    conf.showAndWait();

                    if (conf.getResult() != ButtonType.APPLY) {
                        parent.setScene(scene);
                        parent.centerOnScreen();
                        return;
                    }

                    for (Appointment appt : Database.refreshApptList()) {
                        if (!((zoned_start.toLocalDateTime().isBefore(appt.getStart()) && (zoned_end.toLocalDateTime().isBefore(appt.getEnd())))
                                || ((zoned_start.toLocalDateTime().isAfter(appt.getStart())) && (zoned_end.toLocalDateTime().isAfter(appt.getEnd()))))) {
                            err.setContentText(bun.getString("Overlapping"));
                            err.showAndWait();
                            return;
                        }
                    }

                    int cid = Database.matchCustomer(customer.getSelectionModel().getSelectedItem()).getCustomerID();
                    int ccid = Database.matchContact(contact.getSelectionModel().getSelectedItem()).contactID();
                    Appointment aa = new Appointment(a.getAppointmentID(), title.getText(), desc.getText(), location.getText(),
                            type.getText(), ZonedDateTime.of(startd.getValue(), LocalTime.of(utc_start.getHour(), utc_start.getMinute()), ZoneId.of("UTC")).toLocalDateTime(),
                            ZonedDateTime.of(startd.getValue(), LocalTime.of(utc_end.getHour(), utc_end.getMinute()), ZoneId.of("UTC")).toLocalDateTime(), LocalDateTime.now(ZoneId.of("UTC")), a.getCreatedBy(),
                            Timestamp.valueOf(LocalDateTime.now(ZoneId.of("UTC"))), CURRENT_USER,
                            cid, 1, ccid);
                    Database.save(aa);
                    tables.updateApptTableData();
                    inf.setContentText("Appointment has been saved");
                    inf.showAndWait();
                    title.setText("");
                    desc.setText("");
                    type.setText("");
                    location.setText("");
                    startd.setValue(LocalDate.now());
                    startm.getSelectionModel().select(0);
                    starth.getSelectionModel().select(0);
                    endm.getSelectionModel().select(0);
                    endh.getSelectionModel().select(0);
                    customer.getSelectionModel().select(0);
                    contact.getSelectionModel().select(0);
                    tables.display();

                } catch (SQLException | Database.DoesNotExistException ex) {
                    err.setContentText(bun.getString("Other-Error"));
                    err.showAndWait();
                }
            });
            parent.setScene(scene);
            parent.centerOnScreen();
        }
    }

    /**
     * Holds all the necessary functions for adding, updating and displaying the main window.
     */
    public static class tables {
        private static final TableView<Appointment> table = new TableView<>();
        private static final TableView<Appointment> custApptTable = new TableView<>();
        private static final TableView<Customer> table2 = new TableView<>();
        private static ObservableList<Appointment> data = FXCollections.observableArrayList();
        private static ObservableList<Customer> data2 = FXCollections.observableArrayList();
        private static final StackPane root = new StackPane();
        private static final Scene scene = new Scene(root, 1250, 750);
        private static final GridPane tgp = new GridPane();
        private static final GridPane top = new GridPane();
        private static final Button combos = new Button("View Customers");
        private static final RadioButton month = new RadioButton("View By Month");
        private static final RadioButton week = new RadioButton("View By Week");
        private static final ToggleGroup time = new ToggleGroup();
        private static final GridPane bottom = new GridPane();
        private static final Button add = new Button("Add Appointment");
        private static final Button mod = new Button("Modify Appointment");
        private static final Button del = new Button("Delete Appointment");
        private static final Button log = new Button("Log Off");
        private static final Label ti = new Label("Appointments");
        private static final ComboBox<String> contactCombo = new ComboBox<>();
        private static final ObservableList<String> contacts = FXCollections.observableArrayList();
        private static final GridPane apptStats = new GridPane();
        private static final GridPane custStats = new GridPane();
        private static final ScrollPane sp = new ScrollPane();
        private static final ScrollPane spp = new ScrollPane();
        private static int mon;
        private static final Alert conf = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.APPLY, ButtonType.CANCEL);
        private static final Alert err = new Alert(Alert.AlertType.ERROR, "", ButtonType.CLOSE);
        private static final Alert inf = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.OK);
        private static int FOCUS = 0;

        /**
         * Updates the table data for the appointment table based off the radial selector for month vs week views.
         * @lambda The use of lambda here is to filter the list of appointments by week or month based off current date. Then collect them to an ObservableArrayList. it is used like that about 3 times.
         * @lambda other than that it is used to update the custom report defined above. It is used to get a list of dates from appointment, and filter them by month and type to count them.
         */
        public static void updateApptTableData() {
            try {
                data.clear();
                if(mon == 1){
                    Month m = LocalDate.now().getMonth();
                    ObservableList<Appointment> appts = Database.refreshApptList();
                    data = appts.stream().filter(aa -> aa.getStart().getMonth() == m).collect(Collectors.toCollection(FXCollections::observableArrayList));
                } else {

                    int w = LocalDate.now().getDayOfWeek().getValue();
                    int d = LocalDate.now().getDayOfMonth();
                    int e = 7 - w;
                    int s = 7 - e;

                    LocalDate start = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), d - s);
                    LocalDate end = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), d + e);
                    data = Database.refreshApptList().stream().filter(aa -> aa.getStart().isAfter(start.atTime(0, 0)) && aa.getEnd().isBefore(end.atTime(23, 0))).collect(Collectors.toCollection(FXCollections::observableArrayList));
                }

                table.setItems(data);

            } catch (SQLException e){
                err.setContentText("An error has occured. Please try again later.");
                err.showAndWait();
                login.logoff();
            }

            int i = 0;

            ObservableList<String> types = Database.getAppStr(Appointment::getType);
            types = types.stream().distinct().collect(Collectors.toCollection(FXCollections::observableArrayList));

            apptStats.getChildren().clear();

            for (String t : types) {
                Label lab = new Label(t);
                lab.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
                GridPane.setMargin(lab, new Insets(10));
                GridPane.setColumnIndex(lab, i);
                apptStats.getChildren().add(lab);
                i++;
            }

            i = 0;

            for(String t: types) {
                ColumnConstraints cc = new ColumnConstraints();
                cc.setHalignment(HPos.CENTER);
                long count = types.stream().filter(s -> s.equals(t)).count();
                Label lab = new Label(String.valueOf(count));
                lab.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
                GridPane.setColumnIndex(lab, i);
                GridPane.setRowIndex(lab, 1);
                apptStats.getChildren().add(lab);
                apptStats.getColumnConstraints().add(cc);
                i++;
            }

            i = 0;

            for (Month m : Month.values()) {
                Label lab = new Label(m.name());
                lab.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
                GridPane.setMargin(lab, new Insets(50, 10, 10, 10));
                GridPane.setColumnIndex(lab, i);
                GridPane.setRowIndex(lab, 2);
                apptStats.getChildren().add(lab);

                long lll = Database.getAppLdt(Appointment::getStart).stream().filter(ldt -> ldt.getMonth().equals(m)).count();
                Label labb = new Label(String.valueOf(lll));
                labb.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
                GridPane.setMargin(labb, new Insets(10));
                GridPane.setColumnIndex(labb, i);
                GridPane.setRowIndex(labb, 3);
                apptStats.getChildren().add(labb);

                i++;
            }

            sp.setContent(apptStats);

            sp.setVisible(false);
            GridPane.setMargin(sp, new Insets(0, 100, 0, 100));

            spp.setVisible(false);
            GridPane.setMargin(spp, new Insets(0, 100, 0, 100));

        }

        /**
         * Updates the customer table data.
         * @lambda the lambda used here is used to convert the division ids from the customer class to country ids and collect them to a list for counting later.
         */
        public static void updateCustTableData() {

            try {
                data2 = Database.refreshCustList();
                table2.setItems(data2);
                appointmentWin.updateCustomers();
            } catch (SQLException e){
                err.setContentText("An error has occured. Please try again later.");
                err.showAndWait();
                login.logoff();
            }
            int i = 0;

            custStats.getChildren().clear();

            for (Country t : Database.getCounrtyList()) {
                Label lab = new Label(t.country());
                lab.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
                GridPane.setMargin(lab, new Insets(10));
                GridPane.setColumnIndex(lab, i);
                custStats.getChildren().add(lab);
                i++;
            }

            i = 0;

            spp.setContent(custStats);

            ObservableList<String> ar = Database.getCustStr(c -> {
                try {
                    return Database.country_from_div(c.getDivisionID()).country();
                } catch (SQLException e) {
                    err.setContentText("An error has occured. Please try again later.");
                    err.showAndWait();
                    login.logoff();
                }
                return null;
            });

            for(Country t: Database.getCounrtyList()) {
                ColumnConstraints cc = new ColumnConstraints();
                cc.setHalignment(HPos.CENTER);
                long count = ar.stream().filter(c -> t.country().equals(c)).count();
                Label lab = new Label(String.valueOf(count));
                lab.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));
                GridPane.setColumnIndex(lab, i);
                GridPane.setRowIndex(lab, 1);
                custStats.getChildren().add(lab);
                custStats.getColumnConstraints().add(cc);
                i++;
            }
        }

        /**
         * sets the necessary variables and buttons for the main window with the tables.
         * @lambda All lambda used here are used to set actions for buttons, menu items, combo boxes, etc.
         */
        public static void init() {

            updateApptTableData();
            updateCustTableData();

            mon = 1;

            conf.setTitle("Save?");
            err.setTitle("Error");
            inf.setTitle("Confirmed");


            MenuItem menuItem1 = new MenuItem("View Appointment Table");
            MenuItem menuItem2 = new MenuItem("View Customer Table");
            MenuItem menuItem3 = new MenuItem("View Contact Schedule");
            MenuItem menuItem4 = new MenuItem("View Appointment Statistics");
            MenuItem menuItem5 = new MenuItem("View Customer Statistics");

            MenuButton menu = new MenuButton("...", null, menuItem1, menuItem2, menuItem3, menuItem4, menuItem5);

            menuItem1.setOnAction(e -> {
                table.setVisible(true);
                table2.setVisible(false);
                custApptTable.setVisible(false);
                contactCombo.setVisible(false);
                contactCombo.setVisible(false);
                week.setVisible(true);
                month.setVisible(true);
                add.setText("Add Appointment");
                mod.setText("Modify Appointment");
                del.setText("Delete Appointment");
                ti.setText("Appointments");
                add.setVisible(true);
                mod.setVisible(true);
                del.setVisible(true);
                sp.setVisible(false);
                spp.setVisible(false);
                FOCUS = 0;
            });

            menuItem2.setOnAction(e -> {
                table.setVisible(false);
                table2.setVisible(true);
                custApptTable.setVisible(false);
                contactCombo.setVisible(false);
                contactCombo.setVisible(false);
                week.setVisible(false);
                month.setVisible(false);
                add.setText("Add Customer");
                mod.setText("Modify Customer");
                del.setText("Delete Customer");
                ti.setText("Customers");
                add.setVisible(true);
                mod.setVisible(true);
                del.setVisible(true);
                sp.setVisible(false);
                spp.setVisible(false);
                FOCUS = 1;
            });

            menuItem3.setOnAction(e -> {
                table.setVisible(false);
                table2.setVisible(false);
                custApptTable.setVisible(true);
                contactCombo.setVisible(true);
                contactCombo.setVisible(true);
                week.setVisible(false);
                month.setVisible(false);
                ti.setText("Contact Appointments");
                add.setVisible(false);
                mod.setVisible(false);
                del.setVisible(false);
                sp.setVisible(false);
                spp.setVisible(false);
                FOCUS = 3;
            });

            menuItem4.setOnAction(e -> {
                table.setVisible(false);
                table2.setVisible(false);
                custApptTable.setVisible(false);
                contactCombo.setVisible(false);
                contactCombo.setVisible(false);
                week.setVisible(false);
                month.setVisible(false);
                ti.setText("Appointment Totals");
                add.setVisible(false);
                mod.setVisible(false);
                del.setVisible(false);
                sp.setVisible(true);
                spp.setVisible(false);
                FOCUS = 3;
            });

            menuItem5.setOnAction(e -> {
                table.setVisible(false);
                table2.setVisible(false);
                custApptTable.setVisible(false);
                contactCombo.setVisible(false);
                contactCombo.setVisible(false);
                week.setVisible(false);
                month.setVisible(false);
                ti.setText("Appointment Totals");
                add.setVisible(false);
                mod.setVisible(false);
                del.setVisible(false);
                sp.setVisible(false);
                spp.setVisible(true);
                FOCUS = 3;
            });



            ObservableList<String> l = FXCollections.observableArrayList();

            ObservableList<Contact> ll = Database.getContact_list();

            for (Contact a : ll) {
                l.add(a.contactName());
            }

            contactCombo.setItems(l);

            contactCombo.setOnAction(e -> custApptTable.setItems(Database.getApptsFromContact(contactCombo.getSelectionModel().getSelectedItem())));

            ObservableList<Contact> cl = Database.getContact_list();
            for (Contact c : cl) {
                contacts.add(c.contactName());
            }

            contactCombo.setVisible(false);

            contactCombo.getSelectionModel().select(0);

            combos.setOnAction(e -> {
                if (FOCUS == 1) {
                    table.setVisible(true);
                    table2.setVisible(false);

                } else if (FOCUS == 0) {
                    table.setVisible(false);
                    table2.setVisible(true);

                }
            });

            log.setOnAction(e -> login.logoff());

            add.setOnAction(e -> {
                if(FOCUS == 0) {
                    appointmentWin.displayAdd();
                } else {
                    customerWin.displayAdd();
                }
            });

            del.setOnAction(e -> {
                if(table.getSelectionModel().getSelectedIndex() != -1 && FOCUS == 0) {
                    Appointment app = table.getSelectionModel().getSelectedItem();
                    int id = app.getAppointmentID();
                    String type = app.getType();
                    conf.setContentText("Remove Appointment " + table.getSelectionModel().getSelectedItem().getTitle() + "?");
                    conf.showAndWait();
                    if (conf.getResult() == ButtonType.APPLY) {
                        try {
                            Database.remove(table.getSelectionModel().getSelectedItem());
                            updateApptTableData();
                            inf.setContentText(type + " Appointment with ID " + id + "been removed");
                            inf.showAndWait();
                        } catch (SQLException ignored) {
                            err.setContentText(bun.getString("AppRemove"));
                            err.showAndWait();
                        }
                    }

                } else if(table2.getSelectionModel().getSelectedIndex() != -1 && FOCUS == 1) {
                    if(Database.customerAppointments(table2.getSelectionModel().getSelectedItem())) {
                        conf.setContentText("Remove Cuatomer " + table2.getSelectionModel().getSelectedItem().getCustomerName() + "?");
                        conf.showAndWait();
                        if(conf.getResult() == ButtonType.APPLY) {
                            try {
                                Database.remove(table2.getSelectionModel().getSelectedItem());
                                inf.setContentText("Customer has been removed");
                                inf.showAndWait();
                                updateCustTableData();
                                appointmentWin.updateCustomers();
                            } catch (SQLException ignored) {
                                err.setContentText(bun.getString("CusRemove"));
                                err.showAndWait();
                            }
                        }
                    } else {
                        err.setContentText(bun.getString("AssociatedApp"));
                        err.showAndWait();
                    }
                } else {
                    err.setContentText(bun.getString("Selection"));
                    err.showAndWait();
                }
            });

            mod.setOnAction(e -> {
                if(FOCUS == 0 && table.getSelectionModel().getSelectedIndex() != -1) {
                    try {
                        appointmentWin.displayMod(table.getSelectionModel().getSelectedItem());
                    } catch (SQLException | Database.DoesNotExistException ex) {
                        err.setContentText(bun.getString("Other-Error"));
                        err.showAndWait();
                    }
                } else if(table2.getSelectionModel().getSelectedIndex() != -1) {
                    try {
                        customerWin.displayMod(table2.getSelectionModel().getSelectedItem());
                    } catch (SQLException ex) {
                        err.setContentText(bun.getString("Other-Error"));
                        err.showAndWait();
                    }
                } else {
                    err.setContentText(bun.getString("Selection"));
                    err.showAndWait();
                }
            });

            month.setToggleGroup(time);
            week.setToggleGroup(time);

            month.setOnAction(e -> {
                mon = 1;
                updateApptTableData();
            });

            week.setOnAction(e -> {
                mon = 0;
                updateApptTableData();
            });

            month.setSelected(true);

            custApptTable.setEditable(false);

            TableColumn<Appointment, String> capptid = new TableColumn<>("Appointment ID");
            capptid.setMinWidth(100);
            capptid.setCellValueFactory(
                    new PropertyValueFactory<>("appointmentID"));

            TableColumn<Appointment, String> ctitle = new TableColumn<>("Title");
            ctitle.setMinWidth(100);
            ctitle.setCellValueFactory(
                    new PropertyValueFactory<>("title"));

            TableColumn<Appointment, String> cdes = new TableColumn<>("Description");
            cdes.setMinWidth(100);
            cdes.setCellValueFactory(
                    new PropertyValueFactory<>("desc"));

            TableColumn<Appointment, String> cloc = new TableColumn<>("Location");
            cloc.setMinWidth(100);
            cloc.setCellValueFactory(
                    new PropertyValueFactory<>("location"));

            TableColumn<Appointment, String> ccont = new TableColumn<>("Contact");
            ccont.setMinWidth(100);
            ccont.setCellValueFactory(
                    new PropertyValueFactory<>("contactID"));

            TableColumn<Appointment, String> cty = new TableColumn<>("Type");
            cty.setMinWidth(100);
            cty.setCellValueFactory(
                    new PropertyValueFactory<>("type"));

            TableColumn<Appointment, String> cst = new TableColumn<>("Start");
            cst.setMinWidth(100);
            cst.setCellValueFactory(
                    new PropertyValueFactory<>("start"));

            TableColumn<Appointment, String> ced = new TableColumn<>("End");
            ced.setMinWidth(100);
            ced.setCellValueFactory(
                    new PropertyValueFactory<>("end"));

            TableColumn<Appointment, String> ccid = new TableColumn<>("Customer ID");
            ccid.setMinWidth(100);
            ccid.setCellValueFactory(
                    new PropertyValueFactory<>("customerID"));

            TableColumn<Appointment, String> cuid = new TableColumn<>("User ID");
            cuid.setMinWidth(100);
            cuid.setCellValueFactory(
                    new PropertyValueFactory<>("userID"));

            custApptTable.getColumns().addAll(Arrays.asList(capptid, ctitle, cdes, cloc, ccont, cty, cst, ced, ccid, cuid));

            GridPane.setMargin(custApptTable, new Insets(0, 100, 0, 100));

            custApptTable.setVisible(false);

            custApptTable.setItems(Database.getApptsFromContact(ll.get(0).contactName()));

            table.setEditable(false);

            TableColumn<Appointment, String> apptid = new TableColumn<>("Appointment ID");
            apptid.setMinWidth(100);
            apptid.setCellValueFactory(
                    new PropertyValueFactory<>("appointmentID"));

            TableColumn<Appointment, String> title = new TableColumn<>("Title");
            title.setMinWidth(100);
            title.setCellValueFactory(
                    new PropertyValueFactory<>("title"));

            TableColumn<Appointment, String> des = new TableColumn<>("Description");
            des.setMinWidth(100);
            des.setCellValueFactory(
                    new PropertyValueFactory<>("desc"));

            TableColumn<Appointment, String> loc = new TableColumn<>("Location");
            loc.setMinWidth(100);
            loc.setCellValueFactory(
                    new PropertyValueFactory<>("location"));

            TableColumn<Appointment, String> cont = new TableColumn<>("Contact");
            cont.setMinWidth(100);
            cont.setCellValueFactory(
                    new PropertyValueFactory<>("contactID"));

            TableColumn<Appointment, String> ty = new TableColumn<>("Type");
            ty.setMinWidth(100);
            ty.setCellValueFactory(
                    new PropertyValueFactory<>("type"));

            TableColumn<Appointment, String> st = new TableColumn<>("Start");
            st.setMinWidth(100);
            st.setCellValueFactory(
                    new PropertyValueFactory<>("start"));

            TableColumn<Appointment, String> ed = new TableColumn<>("End");
            ed.setMinWidth(100);
            ed.setCellValueFactory(
                    new PropertyValueFactory<>("end"));

            TableColumn<Appointment, String> cid = new TableColumn<>("Customer ID");
            cid.setMinWidth(100);
            cid.setCellValueFactory(
                    new PropertyValueFactory<>("customerID"));

            TableColumn<Appointment, String> uid = new TableColumn<>("User ID");
            uid.setMinWidth(100);
            uid.setCellValueFactory(
                    new PropertyValueFactory<>("userID"));

            table.setItems(data);
            table.getColumns().addAll(Arrays.asList(apptid, title, des, loc, cont, ty, st, ed, cid, uid));

            GridPane.setMargin(table, new Insets(0, 100, 0, 100));

            ///// Table 2

            table2.setEditable(false);

            TableColumn<Customer, String> custid = new TableColumn<>("Customer ID");
            custid.setMinWidth(100);
            custid.setCellValueFactory(
                    new PropertyValueFactory<>("customerID"));

            TableColumn<Customer, String> cmne = new TableColumn<>("Name");
            cmne.setMinWidth(100);
            cmne.setCellValueFactory(
                    new PropertyValueFactory<>("customerName"));

            TableColumn<Customer, String> addr = new TableColumn<>("Address");
            addr.setMinWidth(100);
            addr.setCellValueFactory(
                    new PropertyValueFactory<>("address"));

            TableColumn<Customer, String> post = new TableColumn<>("Postal Code");
            post.setMinWidth(100);
            post.setCellValueFactory(
                    new PropertyValueFactory<>("postalCode"));

            TableColumn<Customer, String> phone = new TableColumn<>("Phone");
            phone.setMinWidth(100);
            phone.setCellValueFactory(
                    new PropertyValueFactory<>("phone"));

            TableColumn<Customer, String> create = new TableColumn<>("Create Date");
            create.setMinWidth(100);
            create.setCellValueFactory(
                    new PropertyValueFactory<>("createDate"));

            TableColumn<Customer, String> createb = new TableColumn<>("Created By");
            createb.setMinWidth(100);
            createb.setCellValueFactory(
                    new PropertyValueFactory<>("createdBy"));

            TableColumn<Customer, String> lasup = new TableColumn<>("Last Update");
            lasup.setMinWidth(100);
            lasup.setCellValueFactory(
                    new PropertyValueFactory<>("lastUpdate"));

            TableColumn<Customer, String> lasupb = new TableColumn<>("Last Updated by");
            lasupb.setMinWidth(100);
            lasupb.setCellValueFactory(
                    new PropertyValueFactory<>("lastUpdatedBy"));

            TableColumn<Customer, String> divid = new TableColumn<>("Division ID");
            divid.setMinWidth(100);
            divid.setCellValueFactory(
                    new PropertyValueFactory<>("divisionID"));

            table2.setItems(data2);
            table2.getColumns().addAll(Arrays.asList(custid, cmne, addr, post, phone, create, createb, lasup, lasupb, divid));

            GridPane.setMargin(table2, new Insets(0, 100, 0, 100));

            table2.setVisible(false);


            StackPane.setMargin(tgp, new Insets(100));


            StackPane.setMargin(tgp, new Insets(100));

            Insets ins = new Insets(10, 100, 10, 100);

            GridPane.setMargin(top, ins);
            GridPane.setMargin(bottom, ins);


            StackPane.setAlignment(root, Pos.CENTER);

            tgp.setAlignment(Pos.CENTER);

            GridPane.setRowIndex(table, 1);
            GridPane.setRowIndex(table2, 1);
            GridPane.setRowIndex(custApptTable, 1);
            GridPane.setRowIndex(sp, 1);
            GridPane.setRowIndex(spp, 1);



            ti.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12));

            GridPane.setColumnIndex(ti, 0);
            GridPane.setColumnIndex(week, 1);
            GridPane.setColumnIndex(month, 2);
            GridPane.setColumnIndex(contactCombo, 2);
            GridPane.setColumnIndex(menu, 3);

            GridPane g = new GridPane();

            ColumnConstraints cc = new ColumnConstraints();

            cc.prefWidthProperty().bind(tgp.widthProperty());
            cc.setHalignment(HPos.CENTER);

            top.getColumnConstraints().addAll(cc, cc, cc, cc);


            top.getChildren().addAll(ti, contactCombo, week, month, menu);

            GridPane.setRowIndex(table, 1);
            GridPane.setRowIndex(bottom, 2);


            tgp.getChildren().addAll(top, table, table2, custApptTable, sp, spp, bottom);
            tgp.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(10), new BorderWidths(2))));

            GridPane.setColumnIndex(add, 0);
            GridPane.setColumnIndex(mod, 1);
            GridPane.setColumnIndex(del, 2);
            GridPane.setColumnIndex(log, 3);
            bottom.getChildren().addAll(add, mod, del, log);
            bottom.getColumnConstraints().addAll(cc, cc, cc, cc);


            g.getChildren().addAll(tgp);

            GridPane.setColumnIndex(tgp, 0);

            g.setAlignment(Pos.CENTER);

            root.getChildren().addAll(g);
        }

        /**
         * displays the main window and tables.
         */
        public static void display() {

            parent.setScene(scene);
            parent.centerOnScreen();
        }

        /**
         * clears the table data for logging off.
         */
        public static void logoff() {
            data.clear();
            data2.clear();
        }
    }

    /**
     * The start of the javafx application.
     * @param stage Unused.
     */
    @Override
    public void start(Stage stage) {
        parent = new Stage();
        try {
            Database.connect();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        login.init();
        login.display();
        parent.show();
    }

    /**
     * main
     * @param args Arguments, unused.
     */
    public static void main(String[] args) {
        launch();
    }
}
