package software.DMS;

import java.time.LocalDateTime;

/**
 * This class has interfaces for the lambda used for the appointment class functions in the database class.
 */
public class AppointmentFunction {
    /**
     * an interface of functions that return strings.
     */
    public interface Str {
        /**
         * Is used for lambda functions for filtering the appointment list more efficiently.
         * @param a an appointment.
         * @return a string.
         */
        String strFn(Appointment a);
    }

    /**
     * interface for returning LocalDateTimes.
     */
    public interface ldt {
        /**
         * Is used for lambda functions for filtering the appointment list more efficiently.
         * @param a an appointment.
         * @return a LocalDateTime.
         */
        LocalDateTime ldtFn(Appointment a);
    }
}
