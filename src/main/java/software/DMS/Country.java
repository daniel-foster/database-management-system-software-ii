package software.DMS;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * A record of country data queried from the database.
 * @param countryID
 * @param country
 * @param createDate
 * @param createdBy
 * @param lastUpdate
 * @param lastUpdatedBy
 */
public record Country(int countryID, String country, LocalDateTime createDate, String createdBy, Timestamp lastUpdate,
                      String lastUpdatedBy) {
}
