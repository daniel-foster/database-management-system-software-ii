package software.DMS;

/**
 * This class has interfaces for the lambda used for the customer class functions in the database class.
 */
public class CustomerFunction {
    /**
     * interfaces that return strings
     */
    public interface Str {
        /**
         * Is used for lambda functions for filtering the appointment list more efficiently.
         * @param c a customer.
         * @return a string.
         */
        String strFn(Customer c);
    }
}
