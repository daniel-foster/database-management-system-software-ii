package software.DMS;

/**
 * A record of contact data queried from the database.
 * @param contactID
 * @param contactName
 * @param email
 */
public record Contact(int contactID, String contactName, String email) {
}
