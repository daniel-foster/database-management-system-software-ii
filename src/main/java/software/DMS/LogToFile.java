package software.DMS;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class for writing things to a file.
 */
public class LogToFile {

    /**
     * Save a string to a file.
     * @param s the line to write.
     * @param f the file to write to.
     * @throws IOException if f cannot be written to.
     */
    public static void save(String s, File f) throws IOException {
        BufferedWriter w = new BufferedWriter(new FileWriter(f, true));
        w.write(s);
        w.newLine();
        w.close();
    }
}
